package com.itheima.sh.order.controller;

import com.itheima.sh.order.pojo.Order;
import com.itheima.sh.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("findOrderByUsername")
    public List<Order> findOrderByUsername(@RequestParam("username") String username){
        return orderService.findOrderByUsername(username);
    }

}
