package com.itheima.sh.order.pojo;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Description:
 * @Version: V1.0
 */
@Data
@Table(name = "tb_order")  // 数据库表和实体类之间的关联
public class Order {


    @Id   // 主键
    private String id;

    //@Column(name = "total_num")
    private Integer totalNum;

    private Integer payMoney;

    private String payType;

    private Date createTime;

    private String username;

    private String receiverContact;

    private String receiverMobile;

    private String receiverAddress;

    private String sourceType;

    private String orderStatus;

    private String payStatus;

    private String isDelete;

    private static final long serialVersionUID = 1L;

}