package com.itheima.sh.order.service;

import com.itheima.sh.order.mapper.OrderMapper;
import com.itheima.sh.order.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    public List<Order> findOrderByUsername(String username){

        //参数校验
        if(username==null){
            throw new RuntimeException("参数非法");
        }

        Example example = new Example(Order.class);

        Example.Criteria criteria=example.createCriteria();
        //参数1：实体类中属性名称
      /*  if(username!=null){
            criteria.andEqualTo("username",username);
        }*/

        criteria.andEqualTo("username",username);

        List<Order> orderList = orderMapper.selectByExample(example);
        if(orderList==null){
            return new ArrayList<>();
        }

        return orderList;
    }
}
