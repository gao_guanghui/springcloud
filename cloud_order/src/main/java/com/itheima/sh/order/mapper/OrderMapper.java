package com.itheima.sh.order.mapper;

import com.itheima.sh.order.pojo.Order;
import tk.mybatis.mapper.common.Mapper;

public interface OrderMapper extends Mapper<Order> {

}
