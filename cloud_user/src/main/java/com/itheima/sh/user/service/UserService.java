package com.itheima.sh.user.service;

import com.itheima.sh.user.mapper.UserMapper;
import com.itheima.sh.user.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.Mapper;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;


    public User findUserById(String id){
        return userMapper.selectByPrimaryKey(id);
    }

}
