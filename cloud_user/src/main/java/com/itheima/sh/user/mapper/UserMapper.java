package com.itheima.sh.user.mapper;

import com.itheima.sh.user.pojo.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {

}
