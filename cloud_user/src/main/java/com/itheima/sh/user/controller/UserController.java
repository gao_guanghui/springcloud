package com.itheima.sh.user.controller;

import com.itheima.sh.user.pojo.User;
import com.itheima.sh.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("findUserById/{username}")
    public User findUserById(@PathVariable("username") String id){
         return userService.findUserById(id);
    }


}
