package com.itheima.sh.user.pojo;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "tb_user")
//@ApiModel("User：用户信息")
public class User {
    @Id
 //   @ApiModelProperty("用户登录名")
    private String username;

    //    @Column
  //  @ApiModelProperty("密码")
    private String password;

  //  @ApiModelProperty("手机号")
    private String phone;

    private String email;

    private Date created;

    private Date updated;

    private String sourceType;

    private String nickName;

    private String name;

    private String status;

    private String headPic;

    private String qq;

    private String isMobileCheck;

    private String isEmailCheck;

    private String sex;

    private Integer userLevel;

    private Integer points;

    private Integer experienceValue;

    private Date birthday;

    private Date lastLoginTime;

    private static final long serialVersionUID = 1L;
}
